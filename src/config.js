const config = {
  appName: 'Issue Tracker - Redux Demo',
  issueStatuses: ['New', 'In Progress', 'Rejected', 'Resolved']
}

export default config
